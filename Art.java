import java.awt.Color;

public class Art{
	public static void main (String[] args){

		
		Color black = new Color(0, 255, 0);
		double x = .5;
		double y = 1;
		StdDraw.setPenColor(new Color(0,0,255));
		galaxy(x,y);
	}

	//draws a single star to the screen
	public static void star(double x, double y){		
		// double xArray[] = {x, x+.5, x, x-.5}; 
		// double yArray[] = {y ,y ,y+1, y };
		double xArray[] = {x, x-.4, x+.5, x-.5, x+.4 }; 
		double yArray[] = {y ,y-1 ,y-.3, y-.3, y-1 };
		StdDraw.filledPolygon(xArray, yArray);
		StdDraw.setPenColor(new Color(0,255,0));		
	}

	//recursively creates stars to form an image
	public static void galaxy(double x, double y){

		star(x,y);
	}
}