/*	Author: Purnell Jones
*	File: 	Sierpinksi.java
*	Course: COP3502
*	Description: Recursively draw out the Sierpinksi Triangle given a positive int value
*/

import java.awt.Color;

public class Sierpinksi{
	public static void main (String[] args){

		int triangleNumber = Integer.parseInt(args[0]);
		double x = .5;
		double y = 0;
		double len = .5;
		
		drawSierpinski(x,y,len,triangleNumber);
	}


	public static void filledTriangle(double x, double y, double len){
		double xArray[] = { x, x-(len/2), x+(len/2)};
		double yArray[] = { y, y + Math.sqrt(3)*len/2, y + Math.sqrt(3)*len/2};
		Color black = new Color(0,0,0);
 		StdDraw.setPenColor(black);
		StdDraw.filledPolygon(xArray, yArray);
	}
	
	public static void drawSierpinski( double x, double y, double len, int n ){
		
		if ( n == 0 ){
			return;
		}
		else{	
			filledTriangle(x, y, len);
			drawSierpinski(x+(len/2), y ,len/2, n-1);
			drawSierpinski(x-(len/2), y ,len/2, n-1);
			drawSierpinski(x, y + (Math.sqrt(3)*len/2) , len/2, n-1 );
		}
	}
}


